variable "azs" {}

variable "project_name" {}

variable "cluster_name" {}

variable "k8s_version" {}

variable "cluster_vpc" {}

variable "private_subnets" {}

variable "environment" {}

variable "eks_cluster" {}

variable "eks_cluster_sg" {}

variable "nodes_instances_sizes" {}

variable "auto_scale_options" {}

variable "auto_scale_cpu" {}