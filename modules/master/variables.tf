variable "azs" {}

variable "project_name" {}

variable "cluster_name" {}

variable "k8s_version" {}

variable "cluster_vpc" {}

variable "private_subnets" {}

variable "environment" {}