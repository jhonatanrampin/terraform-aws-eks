output "eks_cluster_id" {
  value = aws_eks_cluster.eks_cluster.id
}

output "security_group_id" {
  value = aws_security_group.cluster_master_sg.id
}