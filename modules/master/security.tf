resource "aws_security_group" "cluster_master_sg" {

  name = "${var.project_name}-${var.environment}-${var.cluster_name}-sg"
  vpc_id = var.cluster_vpc

  egress {
    from_port   = 0
    to_port     = 0

    protocol = "-1"
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  tags = {
    Name = "${var.project_name}-${var.environment}-${var.cluster_name}-sg"
    Environment = var.environment
    Description = "Managed by Terraform"
  }

}

resource "aws_security_group_rule" "cluster_ingress_https" {
  cidr_blocks = ["150.107.175.225/32"]
  from_port   = 443
  to_port     = 443
  protocol    = "tcp"

  security_group_id = aws_security_group.cluster_master_sg.id
  type = "ingress"
}