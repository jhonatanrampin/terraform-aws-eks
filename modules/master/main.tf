resource "aws_eks_cluster" "eks_cluster" {
  name    = "${var.project_name}-${var.environment}-${var.cluster_name}"
  version = var.k8s_version
  role_arn = aws_iam_role.eks_cluster_role.arn

  vpc_config {

    security_group_ids = [
      aws_security_group.cluster_master_sg.id
    ]

    // Getting private subnets list/size from the VPC module
    subnet_ids = var.private_subnets
  }

  tags = {
    // This tag is mandatory - DO NOT remove it from here.
    "kubernetes.io/cluster/${var.project_name}-${var.environment}-${var.cluster_name}" = "shared"
  }

}