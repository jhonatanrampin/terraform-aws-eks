variable "aws_account_id" {
  default     = "123456"
  description = "Set the AWS account id; uses include ensuring global uniqueness of S3 bucket names"
  type        = string
}

variable "aws_region" {
  default = "ap-southeast-2"
}

variable "azs" {
  default = ["ap-southeast-2a", "ap-southeast-2c"]
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "project_name" {
  default = "jr-labs"
}

variable "cidr_network" {
  default = "10.0.0.0/16"
}

variable "public_subnets" {
  default = ["10.0.0.0/20", "10.0.16.0/20"]
}

variable "private_subnets" {
  default = ["10.0.48.0/20", "10.0.64.0/20"]
}

variable "k8s_version" {
  default = "1.20"
}

variable "cluster_name" {
  default = "eks-cluster"
}

variable "nodes_instances_size" {
  default = "t3.large"
}

variable "auto_scale_options" {
  default = {
    min     = 2
    max     = 10
    desired = 2
  }
}

variable "auto_scale_cpu" {
  default = {
    scale_up_threshold  = 80
    scale_up_period     = 60
    scale_up_evaluation = 2
    scale_up_cooldown   = 300
    scale_up_add        = 2

    scale_down_threshold  = 40
    scale_down_period     = 120
    scale_down_evaluation = 2
    scale_down_cooldown   = 300
    scale_down_remove     = -1
  }
}