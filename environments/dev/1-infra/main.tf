module "network" {
  source = "../../../modules/vpc"

  aws_account_id  = var.aws_account_id
  project_name    = var.project_name
  aws_region      = var.aws_region
  azs             = var.azs
  environment     = var.environment
  cidr_network    = var.cidr_network

  cluster_name    = var.cluster_name
  public_subnets  = var.public_subnets
  private_subnets = var.private_subnets
}

module "k8s_master" {
  source = "../../../modules/master"

  project_name    = var.project_name
  cluster_name    = var.cluster_name
  azs             = var.azs
  environment     = var.environment
  k8s_version     = var.k8s_version

  cluster_vpc     = module.network.vpc_id
  private_subnets = module.network.private_subnets
}

module "k8s_nodes" {
  source = "../../../modules/nodes"

  project_name          = var.project_name
  cluster_name          = var.cluster_name
  azs                   = var.azs
  environment           = var.environment
  k8s_version           = var.k8s_version

  cluster_vpc           = module.network.vpc_id
  private_subnets       = module.network.private_subnets

  eks_cluster           = module.k8s_master.eks_cluster_id
  eks_cluster_sg        = module.k8s_master.security_group_id

  nodes_instances_sizes = var.nodes_instances_size
  auto_scale_options    = var.auto_scale_options

  auto_scale_cpu        = var.auto_scale_cpu
}