# Metric Server

EKS does not come with metric server by default. Therefore, we have to create these services in order to collect metrics for the HPA.
